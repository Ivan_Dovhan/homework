document.addEventListener('DOMContentLoaded', function () {
    const table = document.createElement('table');
    table.addEventListener('click', toggleCellColor);

    for (let i = 0; i < 30; i++) {
        const row = document.createElement('tr');
        for (let j = 0; j < 30; j++) {
            const cell = document.createElement('td');
            row.appendChild(cell);
        }
        table.appendChild(row);
    }

    document.body.appendChild(table);

    document.body.addEventListener('click', function (event) {
        if (event.target.tagName.toLowerCase() !== 'td') {
            toggleAllCells();
        }
    });

    function toggleCellColor(event) {
        if (event.target.tagName.toLowerCase() === 'td') {
            event.target.classList.toggle('black');
        }
    }

    function toggleAllCells() {
        const cells = document.querySelectorAll('td');
        cells.forEach(cell => {
            cell.classList.toggle('black');
        });
    }
});