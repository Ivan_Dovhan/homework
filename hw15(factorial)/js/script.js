// Отримати число від користувача за допомогою модального вікна браузера
const number = prompt("Введіть число:");

// Функція для підрахунку факторіалу числа
const factorial = (num) => {
  if (num === 0 || num === 1) {
    return 1;
  }
  return num * factorial(num - 1);
};

// Виклик функції та виведення результату на екран
const result = factorial(parseInt(number));
alert(`Факторіал числа ${number} дорівнює ${result}.`);


// Функція для перевірки, чи є передане значення числом
const isNumeric = (value) => {
    return !isNaN(parseFloat(value)) && isFinite(value);
  };
  
  // Отримати число від користувача з перевіркою коректності даних
  let number = prompt("Введіть число:");
  
  while (!isNumeric(number)) {
    number = prompt("Введіть коректне число. Спробуйте знову:");
  }
  
  // Функція для підрахунку факторіалу числа
  const factorial = (num) => {
    if (num === 0 || num === 1) {
      return 1;
    }
    return num * factorial(num - 1);
  };
  
  // Виклик функції та виведення результату на екран
  const result = factorial(parseInt(number));
  alert(`Факторіал числа ${number} дорівнює ${result}.`);