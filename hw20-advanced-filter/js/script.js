function filterCollection(collection, keywords, allKeywords, ...fields) {
    const normalizedKeywords = keywords.toLowerCase().split(' ');

    return collection.filter(item => {
        const checkKeywords = (value) => {
            if (typeof value === 'string') {
                const normalizedValue = value.toLowerCase();
                return allKeywords
                    ? normalizedKeywords.every(keyword => normalizedValue.includes(keyword))
                    : normalizedKeywords.some(keyword => normalizedValue.includes(keyword));
            } else if (Array.isArray(value)) {
                return value.some(checkKeywords);
            } else if (typeof value === 'object' && value !== null) {
                return Object.values(value).some(checkKeywords);
            }
            return false;
        };

        return fields.some(field => checkKeywords(getValueByPath(item, field)));
    });
}

function getValueByPath(obj, path) {
    const keys = path.split('.');
    return keys.reduce((acc, key) => (acc && acc[key] !== undefined ? acc[key] : undefined), obj);
}

const vehicles = [
    {
        name: 'Toyota',
        description: 'Car from Japan',
        contentType: { name: 'Vehicle', type: 'Car' },
        locales: [
            { name: 'en_US', description: 'English (US)' },
            { name: 'es_ES', description: 'Spanish (Spain)' }
        ]
    },
];

const filteredVehicles = filterCollection(
    vehicles,
    'en_US Toyota',
    true,
    'name',
    'description',
    'contentType.name',
    'locales.name',
    'locales.description'
);

console.log(filteredVehicles);