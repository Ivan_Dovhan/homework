import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import { store } from './redux/store/store';
import { AppRouter } from './router/router';

import './styles/style.scss';

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <AppRouter />  {/* Використовуйте компонент AppRouter тут */}
    </Router>
  </Provider>,
  document.getElementById('root')
);