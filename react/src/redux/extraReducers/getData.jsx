import { createAsyncThunk } from '@reduxjs/toolkit';

// url server
const url = '/db/db.json';

// get data from server
export const getData = createAsyncThunk(
  'products/getData',
  async (_, { rejectWithValue }) => {
    try {
      const response = await fetch(url);
      if (!response.ok) throw new Error('Error loading products from server');
      const data = await response.json();
      console.log('Data from server:', data);
      return data;
    } catch (e) {
      return rejectWithValue(e.message);
    }
  }
);
