export const headphones = [
    {
      "id": 1,
      "name": "audi",
      "price": 100000,
      "imgUrl": "1.jpg",
      "article": "p-01254",
      "color": "black"
    },
    {
      "id": 2,
      "name": "BMW",
      "price": 150000,
      "imgUrl": "2.jpg",
      "article": "p-6598",
      "color": "gold"
    },
    {
      "id": 3,
      "name": "chevrole-camaro",
      "price": 200000,
      "imgUrl": "3.jpg",
      "article": "p-659878",
      "color": "carbon"
    },
    {
      "id": 4,
      "name": "dodge",
      "price": 250000,
      "imgUrl": "4.jpg",
      "article": "p-7845",
      "color": "silver"
    },
    {
      "id": 5,
      "name": "Jeep",
      "price": 300000,
      "imgUrl": "5.jpg",
      "article": "p-5582",
      "color": "wood"
    },
    {
      "id": 6,
      "name": "lamborghini",
      "price": 350000,
      "imgUrl": "6.jpg",
      "article": "p-68942",
      "color": "black"
    },
    {
      "id": 7,
      "name": "mazda",
      "price": 400000,
      "imgUrl": "7.jpg",
      "article": "p-32541",
      "color": "black/wood",
    },
    {
      "id": 8,
      "name": "MGL",
      "price": 450000,
      "imgUrl": "8.jpg",
      "article": "p-55487",
      "color": "black"
    },
    {
      "id": 9,
      "name": "rolls-royce",
      "price": 500000,
      "imgUrl": "9.jpg",
      "article": "p-986555",
      "color": "wood"
    },
    {
      "id": 10,
      "name": "volkswagen",
      "price": 550000,
      "imgUrl": "10.jpg",
      "article": "p-665472",
      "color": "white/gold"
    },
  ];
  