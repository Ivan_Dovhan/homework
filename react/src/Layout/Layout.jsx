import React from 'react';
import { Header } from '../components/Header/Header';
import { Main } from '../components/Main/Main';
import { Footer } from '../components/Footer/Footer';
import { MyModal } from '../UI/MyModal/MyModal';
import styles from './Layout.module.scss';

export const Layout = () => {
  return (
    <>
      <header className={styles.Header}>
        <div className={styles.container}>
          <Header />
        </div>
      </header>
      <main className={styles.Main}>
        <div className={styles.container}>
          <Main />
        </div>
      </main>
      <footer className={styles.Footer}>
        <div className={styles.container}>
          <Footer />
        </div>
      </footer>
      <MyModal />
    </>
  );
};

