import React from 'react';
import PropTypes from 'prop-types';
import { useOutletContext } from 'react-router-dom';
import styles from './Favorites.module.scss';
import { Product } from '../../components/Product/Product';

export const Favorites = () => {
  const {
    products,
    favorites,
    handleFavoritesClick,
    productsInBasket,
    handleBasketClick,
    setModal,
    setModalData,
  } = useOutletContext();

  const productsFavorites = products.filter((product) =>
    favorites.includes(product.id)
  );

  const emptyFavorites = !productsFavorites.length;

  return (
    <div className={styles.Favorites}>
      {emptyFavorites && <h1>Доданих товарів немає</h1>}
      {!emptyFavorites && (
        <ul className={styles.FavoritesList}>
          {productsFavorites.map((product) => (
            <Product
              key={product.id}
              product={product}
              favorites={favorites}
              handleFavoritesClick={handleFavoritesClick}
              productsInBasket={productsInBasket}
              handleBasketClick={handleBasketClick}
              setModal={setModal}
              setModalData={setModalData}
            ></Product>
          ))}
        </ul>
      )}
    </div>
  );
};

Favorites.propTypes = {
  product: PropTypes.shape({}).isRequired,
  favorites: PropTypes.array.isRequired,
  handleFavoritesClick: PropTypes.func.isRequired,
  productsInBasket: PropTypes.array.isRequired,
  handleBasketClick: PropTypes.func.isRequired,
  setModal: PropTypes.func.isRequired,
  setModalData: PropTypes.func.isRequired,
};
