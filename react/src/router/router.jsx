import React from 'react';
import { Route, BrowserRouter, Routes } from 'react-router-dom';
import { App } from '../components/App/App';
import { Notfound } from '../pages/Notfound/Notfound';
import { Shop } from '../pages/Shop/Shop';
import { Basket } from '../pages/Basket/Basket';
import { Favorites } from '../pages/Favorites/Favorites';

const AppRoutes = () => (
  <Routes>
    <Route path="/" element={<App />}>
      <Route index element={<Shop />} />
      <Route path="basket" element={<Basket />} />
      <Route path="favorites" element={<Favorites />} />
    </Route>
    <Route path="/*" element={<Notfound />} />
  </Routes>
);

export const AppRouter = () => (
  <BrowserRouter>
    <AppRoutes />
  </BrowserRouter>
);