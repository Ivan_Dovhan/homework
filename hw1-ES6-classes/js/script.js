// 1. Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). 
// Зробіть так, щоб ці характеристики заповнювалися під час створення обєкта.
// 2. Створіть гетери та сеттери для цих властивостей.
// 3. Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
// 4. Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
// 5. Створіть кілька екземплярів обєкта Programmer, виведіть їх у консоль.


class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary
    }

    get name() {
        return this._name;
      }

      set name(newName) {
        this._name = newName;
      }

      get age() {
        return this._age;
      }
      
      set age(newAge) {
        this._age = newAge;
      }
      get salary() {
        return this._salary;
      }
      set salary(newSalary) {
        this._salary = newSalary;
      }
    
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.Land = lang;
    }
    get salary() {
        return this._salary * 3;
      }
   
    
}

const Ivan = new Programmer('Ivan', 25, 50000, ['']);
const Dora = new Programmer('Dora', 30, 70000, ['']);

console.log(Ivan); 
console.log(Dora);

