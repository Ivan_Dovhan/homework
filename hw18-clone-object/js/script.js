function deepClone(obj) {
    if (obj === null || typeof obj !== 'object') {
      return obj;
    }
  
    if (Array.isArray(obj)) {
      const newArray = [];
      for (let i = 0; i < obj.length; i++) {
        newArray[i] = deepClone(obj[i]);
      }
      return newArray;
    }
  
    const newObj = {};
    for (const key in obj) {
      if (obj.hasOwnProperty(key)) {
        newObj[key] = deepClone(obj[key]);
      }
    }
  
    return newObj;
  }
  
  // Приклад використання:
  const originalObject = {
    name: 'John',
    age: 30,
    address: {
      city: 'New York',
      country: 'USA'
    },
    hobbies: ['reading', 'traveling']
  };
  
  const clonedObject = deepClone(originalObject);
  
  console.log(clonedObject);

//   Ця функція deepClone перевіряє, чи є об'єкт не null і не простою змінною, і якщо об'єкт є масивом, вона рекурсивно клонує його елементи. Якщо об'єкт є об'єктом, то також рекурсивно клонує його властивості.