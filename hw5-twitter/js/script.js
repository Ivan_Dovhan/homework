// При відкритті сторінки необхідно отримати з сервера список всіх користувачів та загальний список публікацій. 
// Для цього потрібно надіслати GET запит на наступні дві адреси:
// https://ajax.test-danit.com/api/json/users
// https://ajax.test-danit.com/api/json/posts
// Після завантаження всіх користувачів та їх публікацій необхідно відобразити всі публікації на сторінці.
// Кожна публікація має бути відображена у вигляді картки (приклад: https://prnt.sc/q2em0x),
//  та включати заголовок, текст, а також ім'я, прізвище та імейл користувача,який її розмістив.
// На кожній картці повинна бути іконка або кнопка, яка дозволить видалити цю картку зі сторінки.
//  При натисканні на неї необхідно надіслати DELETE запит на адресу https://ajax.test-danit.com/api/json/posts/${postId}. 
//  Після отримання підтвердження із сервера (запит пройшов успішно), картку можна видалити зі сторінки, використовуючи JavaScript.
// Більш детальну інформацію щодо використання кожного з цих зазначених вище API можна знайти тут.
// Цей сервер є тестовим. Після перезавантаження сторінки всі зміни, які надсилалися на сервер, не будуть там збережені. Це нормально, все так і має працювати.
// Картки обов'язково мають бути реалізовані у вигляді ES6 класів. Для цього необхідно створити клас Card. При необхідності ви можете додавати також інші класи.

class Card {
    constructor(title, text, userName, userEmail, postId) {
        this.title = title;
        this.text = text;
        this.userName = userName;
        this.userEmail = userEmail;
        this.postId = postId
    }

    render() {
        const cardElem = document.createElement('div');
        cardElem.className = 'card';
        cardElem.innerHTML = `
        <h2>${this.title}</h2>
        <p>${this.text}</p>
        <p>Posted by: ${this.userName} (${this.userEmail})</p>
        <span class="delete-btn" data-post-id="${this.postId}">Delete</span>
        `;

        const deleteBtn = cardElem.querySelector('.delete-btn');
        deleteBtn.addEventListener('click', () => {
            this.deletePost();
        });

        return cardElem;
    }

    deletePost() {
        // Відправка DELETE запиту на сервер
        fetch(`https://ajax.test-danit.com/api/json/posts/${this.postId}`, {
            method: 'DELETE'
        })
        .then(response => {
            if (response.ok) {
                // Якщо видалення успішне, видаляємо картку зі сторінки
                const cardElem = document.getElementById(`card-${this.postId}`);
                cardElem.remove();
            } else {
                console.error('Error deleting post:', response.status);
            }
        })
        .catch(error => {
            console.error('Error deleting post:', error);
        });
    }
}

fetch('https://ajax.test-danit.com/api/json/users')
.then(response => response.json())
.then(users => {
    fetch('https://ajax.test-danit.com/api/json/posts')
    .then(response => response.json())
    .then(posts => {
        const cardContainer = document.getElementById('cardContainer');
        posts.forEach(post => {
            const user = users.find(u => u.id === post.userId);
            if (user) {
                const card = new Card(post.title, post.body, user.name, user.email, post.id);
                cardContainer.appendChild(card.render());
            }
        });
    })
    .catch(error => {
        console.error('Error fetching posts:', error);
    });
})
.catch(error => {
    console.error('Error fetching users:', error);
});