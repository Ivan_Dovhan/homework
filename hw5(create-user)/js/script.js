function createNewUser() {
    let firstName = prompt('Enter your first name');
    let lastName = prompt('Enter your name');
    let newUser = {
        firstName,
        lastName,
        getLogin: () => {
            return `${firstName[0]}${lastName}`.toLowerCase();
        }
    }

    return newUser;
}


let newUser = createNewUser();

console.log(newUser.getLogin());

