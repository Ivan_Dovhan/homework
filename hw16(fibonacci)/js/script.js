// Функция для подсчета n-го обобщенного числа Фибоначчи
const generalizedFibonacci = (F0, F1, n) => {
    if (n === 0) {
      return F0;
    } else if (n === 1) {
      return F1;
    } else if (n > 1) {
      let prevPrev = F0;
      let prev = F1;
      let current;
  
      for (let i = 2; i <= n; i++) {
        current = prev + prevPrev;
        prevPrev = prev;
        prev = current;
      }
  
      return current;
    } else if (n < 0) {
      let prevNext = F1;
      let prev = F0;
      let current;
  
      for (let i = -1; i >= n; i--) {
        current = prev - prevNext;
        prevNext = prev;
        prev = current;
      }
  
      return current;
    }
  };
  
  // Считываем число n с помощью модального окна браузера
  const n = parseInt(prompt("Введите порядковый номер числа Фибоначчи (n):"));
  
  // Проверяем корректность введенного числа
  if (isNaN(n)) {
    alert("Введено некорректное значение.");
  } else {
    // Вызываем функцию generalizedFibonacci и выводим результат на экран
    const result = generalizedFibonacci(0, 1, n);
    alert(`Число Фибоначчи с порядковым номером ${n} равно ${result}.`);
  }