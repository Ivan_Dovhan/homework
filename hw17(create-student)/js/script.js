// Створення порожнього об'єкта "студент"
const student = {
    name: "",
    lastName: "",
    grades: [],
  };
  
  // Запитуємо у користувача ім'я та прізвище студента та записуємо їх у відповідні поля об'єкта
  student.name = prompt("Введіть ім'я студента:");
  student.lastName = prompt("Введіть прізвище студента:");
  
  // Запитуємо у користувача назви предметів та оцінки
  let subject = prompt("Введіть назву предмета:");
  while (subject !== null) {
    const grade = parseFloat(prompt(`Введіть оцінку за предмет ${subject}:`));
  
    // Додаємо оцінку до списку оцінок студента
    student.grades.push({ subject, grade });
  
    subject = prompt("Введіть назву наступного предмета (або натисніть Cancel, щоб завершити):");
  }
  
  // Порахунок кількості поганих оцінок (менше 4) та середнього балу з предметів
  let countPoorGrades = 0;
  let sumGrades = 0;
  
  for (const grade of student.grades) {
    sumGrades += grade.grade;
  
    if (grade.grade < 4) {
      countPoorGrades++;
    }
  }
  
  // Виведення результатів аналізу табеля
  if (countPoorGrades === 0) {
    alert("Студента переведено на наступний курс.");
  } else {
    const averageGrade = sumGrades / student.grades.length;
    alert(`Середній бал: ${averageGrade.toFixed(2)}`);
  
    if (averageGrade > 7) {
      alert("Студенту призначено стипендію.");
    }
  }