document.addEventListener('DOMContentLoaded', function () {
    const priceInput = document.getElementById('priceInput');
    const output = document.getElementById('output');

    priceInput.addEventListener('focus', function () {
        priceInput.classList.remove('invalid');
    });

    priceInput.addEventListener('blur', function () {
        const priceValue = parseFloat(priceInput.value);
        if (isNaN(priceValue) || priceValue < 0) {
            priceInput.classList.add('invalid');
            output.innerHTML = '';
            if (isNaN(priceValue)) {
                alert('Please enter a valid number.');
            } else {
                alert('Please enter correct price.');
            }
        } else {
            priceInput.style.borderColor = 'green';
            createOutputSpan(priceValue);
        }
    });

    function createOutputSpan(price) {
        const span = document.createElement('span');
        span.textContent = `Current price: $${price}`;
        const closeButton = document.createElement('button');
        closeButton.textContent = 'X';
        closeButton.addEventListener('click', function () {
            output.removeChild(span);
            output.removeChild(closeButton);
            priceInput.value = '';
            priceInput.style.borderColor = '';
        });

        output.innerHTML = '';
        output.appendChild(span);
        output.appendChild(closeButton);
    }
});