const listKey = document.querySelector('.btn-wrapper');

function handleKey(e) {
  let elemForKey = findElementForKey(e.key.toUpperCase());
  if (!elemForKey) return;
  unpaintAllKey();
  paintKey(elemForKey);
}

function findElementForKey(keyValue) {
  return [...listKey.children].find(elem => elem.textContent.toUpperCase() === keyValue);
}

function paintKey(elemForKey) {
  elemForKey.style.backgroundColor = 'blue';
}

function unpaintAllKey() {
  [...listKey.children].forEach((elem) => {
    elem.removeAttribute('style');
  });
}

document.addEventListener('keydown', handleKey)


