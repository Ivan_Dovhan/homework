const gulp = require('gulp');

// import path
const { path } = require('./gulp/config/path.js');

// import global plugins
const { plugins } = require('./gulp/config/plugins.js');

// Set variable in global object
global.app = {
  isBuild: process.argv.includes('--build'),
  isDev: !process.argv.includes('--build'),
  gulp: gulp,
  path: path,
  plugins: plugins,
};

// Import tasks
const { copy } = require('./gulp/tasks/copy.js');
const { reset } = require('./gulp/tasks/reset.js');
const { html } = require('./gulp/tasks/html.js');
const { server } = require('./gulp/tasks/server.js');
const { scss } = require('./gulp/tasks/scss.js');
const { js } = require('./gulp/tasks/js.js');
const { img } = require('./gulp/tasks/img.js');
const { otfToTtf, ttfToWoff, fontsStyle } = require('./gulp/tasks/fonts.js');

// Watcher
function watcher() {
  gulp.watch(path.watch.files, copy);
  gulp.watch(path.watch.html, html);
  gulp.watch(path.watch.scss, scss);
  gulp.watch(path.watch.js, js);
  gulp.watch(path.watch.img, img);
}

// create fonts
const fonts = gulp.series(otfToTtf, ttfToWoff, fontsStyle);

// main tasks
const mainTasks = gulp.series(fonts, gulp.parallel(copy, html, scss, js, img));

// Tasks execution script
const dev = gulp.series(reset, mainTasks, gulp.parallel(watcher, server));
const build = gulp.series(reset, mainTasks);

// Running a default task
gulp.task('default', dev);

module.exports = {
  dev,
  build,
  reset
};