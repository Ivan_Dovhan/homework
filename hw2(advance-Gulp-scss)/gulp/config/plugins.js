const replace = require('gulp-replace');
const plumber = require('gulp-plumber');
const sourcemaps = require('gulp-sourcemaps');
const gulpIf = require('gulp-if');
const del = require('del');
const browserSync = require('browser-sync').create();
const sass = require('gulp-sass')(require('sass'));
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const rename = require('gulp-rename');
const uglify = require('gulp-uglify');
const imagemin = require('gulp-imagemin');
const ttf2woff = require('gulp-ttf2woff');
const ttf2woff2 = require('gulp-ttf2woff2');
const fonter = require('gulp-fonter');
const fileInclude = require('gulp-file-include');

module.exports = {
  replace,
  plumber,
  sourcemaps,
  gulpIf,
  del,
  browserSync,
  sass,
  autoprefixer,
  cleanCSS,
  rename,
  uglify,
  imagemin,
  ttf2woff,
  ttf2woff2,
  fonter,
  fileInclude,
};
