document.addEventListener('DOMContentLoaded', function () {
    const sliderContainer = document.getElementById('slider-container');
    const sliderImages = document.getElementById('slider-images');
    const prevButton = document.getElementById('prev');
    const nextButton = document.getElementById('next');

    let currentIndex = 0;

    function updateSlider() {
        const translateValue = -currentIndex * 100 + '%';
        sliderImages.style.transform = 'translateX(' + translateValue + ')';
    }

    function nextSlide() {
        currentIndex = (currentIndex + 1) % 6; // Здесь 6 - количество слайдов
        updateSlider();
    }

    function prevSlide() {
        currentIndex = (currentIndex - 1 + 6) % 6; // Здесь 6 - количество слайдов
        updateSlider();
    }

    prevButton.addEventListener('click', prevSlide);
    nextButton.addEventListener('click', nextSlide);
});