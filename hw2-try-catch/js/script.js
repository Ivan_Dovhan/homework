const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];

  for (let i = 0; i < books.length; i++) {
    try {
      const author = books[i].author;
      const name = books[i].name;
      const price = books[i].price;
  
      if (author && name && price) {
        // виконувати додаткові дії з отриманими властивостями
        console.log(`Автор: ${author}, Назва: ${name}, Ціна: ${price}`);
      } else {
        throw new Error('Відсутні необхідні властивості.');
      }
    } catch (error) {
      console.log(`Сталася помилка у книги ${i + 1}:`, error.message);
    }
  }