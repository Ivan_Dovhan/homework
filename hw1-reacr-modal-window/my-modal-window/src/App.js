import React, { useState } from 'react';
import Button from './components/Button';
import Modal from './components/Modal';
import './App.scss';


const App = () => {
  const [isFirstModalOpen, setIsFirstModalOpen] = useState(false);
  const [isSecondModalOpen, setIsSecondModalOpen] = useState(false);

  const openFirstModal = () => {
    setIsFirstModalOpen(true);
  };

  const closeFirstModal = () => {
    setIsFirstModalOpen(false);
  };

  const openSecondModal = () => {
    setIsSecondModalOpen(true);
  };

  const closeSecondModal = () => {
    setIsSecondModalOpen(false);
  };

  const handleOverlayClick = () => {
    if (isFirstModalOpen) closeFirstModal();
    if (isSecondModalOpen) closeSecondModal();
  };

  return (
    <div className="App">
      <h1>React Modal App</h1>
      <Button
        backgroundColor="blue"
        text="Open first modal"
        onClick={openFirstModal}
      />
       <Button
        backgroundColor="green"
        text="Open second modal"
        onClick={openSecondModal}
      />
      
      {isFirstModalOpen && (
        <div className="modal-overlay" onClick={handleOverlayClick}>
          <Modal
            header="Do you want to delete this file?"
            closeButton={true}
            text="Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?"
            actions={
              
              <button className="btn-primary" onClick={closeFirstModal}>
                Cancel
              </button>
              
              
            }
            onClose={closeFirstModal}
          />
        </div>
      )}

{isSecondModalOpen && (
        <div className="modal-overlay" onClick={handleOverlayClick}>
          <Modal
            header="Do you want to save this file?"
            closeButton={true}
            text="As soon as you save the file on your computer, rename it!"
            actions={
              <button className="btn-secondary" onClick={closeSecondModal}>
                Cancel
              </button>
            }
            onClose={closeSecondModal}
          />
        </div>
      )}
    </div>
  );
}
export default App;