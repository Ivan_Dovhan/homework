import React from 'react';

const Modal = ({ header, closeButton, text, actions }) => {
  const handleClose = () => {
    
    // Реалізуйте функціонал для закриття модального вікна
  };

  return (
    <div className="modal">
      <div className="modal-content">
        <div className="modal-header">
          <h3>{header}</h3>
          {closeButton && (
            <button className="close-button" onClick={handleClose}>
              &times;
            </button>
          )}
        </div>
        <div className="modal-body">
          <p>{text}</p>
        </div>
        <div className="modal-footer">
          {actions}
        </div>
      </div>
    </div>
  );
};

export default Modal;