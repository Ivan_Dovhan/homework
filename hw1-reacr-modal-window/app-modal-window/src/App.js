import logo from './logo.svg';
import './App.css';
import React, { useState } from 'react';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;

function App() {
  const [isFirstModalOpen, setIsFirstModalOpen] = useState(false);
  const [isSecondModalOpen, setIsFirstModalOpen] = useState(false);

  const openFirstModal = () => {
    setIsFirstModalOpen(true);
  };

  const closeFirstModal = () => {
    setIsFirstModalOpen(false);
  };

  const openSecondModal = () => {
    setIsSecondModalOpen(true);
  };

  const closeSecondModal = () => {
    setIsSecondModalOpen(false);
  };

  return (
    <div className="App">
      <h1>React Modal App</h1>
      <button onClick={openFirstModal}>Open first modal</button>
      <button onClick={openSecondModal}>Open second modal</button>

      {isFirstModalOpen && (
        <div className="modal">
          <div className="modal-content">
            <h2>First modal</h2>
            <p>This is the first modal window.</p>
            <button onClick={closeFirstModal}>Close</button>
          </div>
        </div>
      )}

      {isSecondModalOpen && (
        <div className="modal">
          <div className="modal-content">
            <h2>Second modal</h2>
            <p>This is the first modal window.</p>
            <button onClick={closeSecondModal}>Close</button>
          </div>
        </div>
      )}
    </div>
  );
}

export default App;