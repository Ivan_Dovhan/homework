import './App.scss';
import Header from "./comonents/Header";
import AppRoutes from "./AppRoutes";
import Modal from './comonents/Modal';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { fetchItems } from './redux/items/actionCreators';
import { fetchFavourite } from './redux/favourite/actionCreators';



const App = () => {

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchItems())
    dispatch(fetchFavourite())
  }, [])

  return (
    <div className="App">
      <Header />
      <section>
        <AppRoutes />
      </section>
    </div>
  );
}

export default App;
