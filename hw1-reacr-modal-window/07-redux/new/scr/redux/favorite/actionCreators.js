import { SET_FAVOURITE, SET_FAVOURITE_LOADING, ADD_FAVOURITE} from "./actions";
import axios from 'axios';

export const setFavourire = (items) => ({ type: SET_FAVOURITE, payload: items });

export const setFavourireLoading = (value) => ({ type: SET_FAVOURITE_LOADING, payload: value });

export const addFavourire = (items) => ({ type: ADD_FAVOURITE, payload: items });


export const fetchFavourite = () => {
    return async (dispatch) => {
        try {
            dispatch(setFavourireLoading(true))
            const { data } = await axios.get('http://localhost:5000/favourites');
            dispatch(setFavourireLoading(false))
            dispatch(setFavourire(data));
        } catch (error) {
            console.log('ERROR', error);
            // dispatch(setError(error.response.data.error));
        }
    }
}


export const postFavourite = (item) => {
    return async (dispatch) => {
        try {
            const { data } = await axios.post('http://localhost:5000/favourites', {
                id: item.id
            });
            dispatch(addFavourire(item));
        } catch (error) {
            console.log('ERROR', error);
            // dispatch(setError(error.response.data.error));
        }
    }
}
