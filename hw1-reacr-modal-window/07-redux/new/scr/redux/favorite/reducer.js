import { SET_FAVOURITE, SET_FAVOURITE_LOADING, ADD_FAVOURITE} from "./actions";

const initialValue = {
    items: [],
    loading: true,
}

const favouriteReducer = (state = initialValue, action) => {
    switch (action.type) {
        case (SET_FAVOURITE): {
            return { ...state, items: action.payload }
        }
        case (SET_FAVOURITE_LOADING): {
            return { ...state, loading: action.payload }
        }

        case (ADD_FAVOURITE): {
            const newItems = [...state.items];
            const item = action.payload;
            newItems.push(item);
            return { ...state, items: newItems }
        }

        default: return state;
    }
}

export default favouriteReducer;