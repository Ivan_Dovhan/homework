import { SET_ITEM, SET_LOADING, SET_ERROR } from "./actions";

const initialValue = {
    items: [],
    loading: true,
    error: ""
}

const itemsReducer = (state = initialValue, action) => {
    switch (action.type) {
        case (SET_ITEM): {
            return { ...state, items: action.payload }
        }
        case (SET_LOADING): {
            return { ...state, loading: action.payload }
        }
        case (SET_ERROR): {
            return { ...state, error: action.payload }
        }

        default: return state;
    }
}

export default itemsReducer;