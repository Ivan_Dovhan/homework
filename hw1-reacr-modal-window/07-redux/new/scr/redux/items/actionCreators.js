import { SET_ITEM, SET_LOADING, SET_ERROR } from "./actions";
import axios from 'axios';

export const setItems = (items) => ({ type: SET_ITEM, payload: items });

export const setLoading = (value) => ({ type: SET_LOADING, payload: value });

export const setError = (error) => ({ type: SET_ERROR, payload: error });


export const fetchItems = () => {
    return async (dispatch) => {
        try {
            const { data } = await axios.get('http://localhost:5000/items');
            dispatch(setItems(data));
            dispatch(setLoading(false))
        } catch (error) {
            console.log('ERROR', error);
            dispatch(setError(error.response.data.error));
            dispatch(setLoading(false))
        }
    }
}
