import styles from './Header.module.scss';
import Nav from "../Nav";
import { useSelector } from 'react-redux';

const Header = () => {

    const favLength = useSelector(state => state.favourite.items.length);
    const favLoading = useSelector(state => state.favourite.loading)

    return (
        <header className={styles.header}>
            <span className={styles.logo}>Logo</span>
            <Nav />

          <div className={styles.quantityWrapper}>
            <span>F: {favLoading ? '...' : favLength}</span>
            <span>C: 0</span>
          </div>
        </header>
    )
}

export default Header;