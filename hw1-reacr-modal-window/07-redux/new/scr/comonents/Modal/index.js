import styles from './Modal.module.scss';
import classNames from "classnames";

const Modal = () => {

  return (
    <div className={styles.root}>
      <div className={styles.background} />
      <div className={styles.contentContainer}>
        <h2>Title</h2>
        <p>Content</p>

        <div className={styles.buttonContainer}>
          <button className={classNames(styles.btn, styles.red)}>Close</button>
        </div>

      </div>
    </div>
  )

}

export default Modal;