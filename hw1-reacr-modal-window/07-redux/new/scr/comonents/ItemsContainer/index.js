import styles from './ItemsContainer.module.scss';
import classNames from "classnames";
import Item from "../Item";
import ItemPlaceholder from '../ItemPlaceholder';
import { useSelector, shallowEqual } from 'react-redux';


const ItemsContainer = () => {

  const items = useSelector((state) => state.items.items, shallowEqual)
  const loading = useSelector((state) => state.items.loading)
  const error = useSelector((state) => state.items.error)

  return (

    <div className={styles.root}>

      {loading && <>
      {/* <ItemPlaceholder />
        <ItemPlaceholder />
        <ItemPlaceholder />
        <ItemPlaceholder />
        <ItemPlaceholder />
        <ItemPlaceholder /> */}
        { new Array(6).fill(null).map((el, index) => <ItemPlaceholder key={index} />) }
      </>}

      {!loading && items.map(({ id, name, price, description, img }) => <Item
        key={id}
        id={id}
        name={name}
        price={price}
        description={description}
        img={img} />)
      }

      {error && <h2 style ={{color: 'red'}} >{error }</h2>}

    </div >
  )
}

export default ItemsContainer;