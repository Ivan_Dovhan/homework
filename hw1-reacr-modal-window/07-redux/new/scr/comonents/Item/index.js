import PropTypes from "prop-types";
import styles from './Item.module.scss';
import { ReactComponent as CartIcon } from '../../assets/svg/cart.svg';
import { ReactComponent as FavouriteIcon } from '../../assets/svg/star.svg';
import classNames from "classnames";
import { postFavourite } from "../../redux/favourite/actionCreators";
import { useDispatch } from "react-redux";

const Item = ({ id, name, price, description, img }) => {

  const dispatch = useDispatch()

  const handleClickFav = () => {
    const cardFav = { id, name, price, description, img }
    dispatch(postFavourite(cardFav))
  }

  return (
    <div className={styles.root}>
      <div className={styles.imgContainer}>
        <img src={img} alt={name} />
      </div>
      <div className={styles.contentWrapper}>
        <div className={styles.info}>
          <div className={styles.name}>{name}</div>
          <div className={styles.price}>{price}$</div>
          <div className={styles.description}>{description}</div>
        </div>

        <div className={styles.btnContainer}>
          <button className={classNames(styles.btn, styles.favouriteBtn)} onClick={handleClickFav}><FavouriteIcon /></button>
          <button className={classNames(styles.btn, styles.cartBtn)}><CartIcon /></button>
        </div>
      </div>

    </div>
  )
}

Item.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string,
  price: PropTypes.number,
  description: PropTypes.string,
  img: PropTypes.string,
};

Item.defaultProps = {
  id: '',
  name: '',
  price: 0,
  description: '',
  img: '',
}

export default Item;