// function displayFilms() {
//   fetch('https://ajax.test-danit.com/api/swapi/films')
//     .then(response => response.json())
//     .then(data => {
//       const filmsList = document.getElementById('films-list');
//       data.forEach(film => {
//         const listItem = document.createElement('li');
//         listItem.innerHTML = `
//           <h2>Episode ${film.episodeId}: ${film.title}</h2>
//           <p>${film.openingCrawl}</p>
//           <ul id="${film.episodeId}-characters"></ul>
//         `;
//         filmsList.appendChild(listItem);
//         displayCharacters(film.characters, film.episodeId);
//       });
//     })
//     .catch(error => console.log(error));
// }

// function displayCharacters(charactersList, episodeId) {
//   const charactersListElement = document.getElementById(`${episodeId}-characters`);
//   charactersList.forEach(characterUrl => {
//     fetch(characterUrl)
//       .then(response => response.json())
//       .then(character => {
//         const listItem = document.createElement('li');
//         listItem.innerHTML = `
//           <h3>${character.name}</h3>
//           <p>Birth Year: ${character.birthYear}</p>
//           <p>Gender: ${character.gender}</p>
//         `;
//         charactersListElement.appendChild(listItem);
//       })
//       .catch(error => console.log(error));
//   });
// }

// displayFilms();


function displayFilms() {
  fetch('https://ajax.test-danit.com/api/swapi/films')
    .then(response => response.json())
    .then(data => {
      const filmsList = document.getElementById('films-list');
      const filmPromises = data.map(film => {
        const listItem = document.createElement('li');
        listItem.innerHTML = `
          <h2>Episode ${film.episodeId}: ${film.title}</h2>
          <p>${film.openingCrawl}</p>
          <ul id="${film.episodeId}-characters"></ul>
        `;
        filmsList.appendChild(listItem);
        return fetchCharacters(film.characters, film.episodeId);
      });
      return Promise.all(filmPromises);
    })
    .catch(error => {
      console.error('Помилка отримання фільмів:', error);
    });
}

function fetchCharacters(charactersList, episodeId) {
  const charactersListElement = document.getElementById(`${episodeId}-characters`);
  const characterPromises = charactersList.map(characterUrl => {
    return fetch(characterUrl)
      .then(response => response.json());
  });

  return Promise.all(characterPromises)
    .then(characters => {
      characters.forEach(character => {
        const listItem = document.createElement('li');
        listItem.innerHTML = `
          <h3>${character.name}</h3>
          <p>Birth Year: ${character.birthYear}</p>
          <p>Gender: ${character.gender}</p>
        `;
        charactersListElement.appendChild(listItem);
      });
    })
    .catch(error => {
      console.error('Помилка отримання персонажів:', error);
    });
}

// Відобразити лоадер при завантаженні даних
function showLoader() {
  const loader = document.createElement('div');
  loader.innerHTML = 'Завантаження...';
  document.body.appendChild(loader);
}

// Приховати лоадер після завантаження даних
function hideLoader() {
  const loader = document.querySelector('div');
  if (loader) {
    loader.remove();
  }
}

showLoader(); // Показати лоадер
displayFilms()
  .then(() => hideLoader()); // Приховати лоадер після завантаження даних

