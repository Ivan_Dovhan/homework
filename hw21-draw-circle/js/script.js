function getRandomColor() {
    const letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

function drawCircle() {
    const diameter = prompt('Введіть діаметр кола (в пікселях):');
    if (!diameter || isNaN(diameter)) {
        alert('Будь ласка, введіть коректне число для діаметра.');
        return;
    }

    const container = document.body;

    for (let i = 0; i < 100; i++) {
        const circle = document.createElement('div');
        circle.className = 'circle';
        circle.style.width = `${diameter}px`;
        circle.style.height = `${diameter}px`;
        circle.style.backgroundColor = getRandomColor();
        circle.onclick = function() {
            container.removeChild(circle);
            updateLayout();
        };

        container.appendChild(circle);
    }

    updateLayout();
}

function updateLayout() {
    const circles = document.querySelectorAll('.circle');
    let leftPosition = 0;

    circles.forEach(circle => {
        circle.style.left = `${leftPosition}px`;
        leftPosition += parseInt(circle.style.width) + 10;
    });
}