document.addEventListener('DOMContentLoaded', function() {
    const minesweeper = document.getElementById('minesweeper');
    const resultDisplay = document.getElementById('result');
    const restartButton = document.getElementById('restart');

    const gridSize = 8;
    const totalMines = 10;

    let mines = [];
    let flags = 0;
    let openedCells = 0;
    let gameOver = false;

    function initializeGame() {
        mines = [];
        flags = 0;
        openedCells = 0;
        gameOver = false;

        resultDisplay.textContent = '';
        restartButton.style.display = 'none';

        for (let i = 0; i < gridSize; i++) {
            const row = document.createElement('div');
            row.className = 'row';

            for (let j = 0; j < gridSize; j++) {
                const cell = document.createElement('div');
                cell.className = 'cell';
                cell.dataset.row = i;
                cell.dataset.col = j;

                cell.addEventListener('click', handleCellClick);
                cell.addEventListener('contextmenu', handleCellRightClick);

                row.appendChild(cell);
            }

            minesweeper.appendChild(row);
        }

        placeMines();
        updateMineCountDisplay();
    }

    function placeMines() {
        while (mines.length < totalMines) {
            const row = Math.floor(Math.random() * gridSize);
            const col = Math.floor(Math.random() * gridSize);

            const duplicateMine = mines.some(mine => mine.row === row && mine.col === col);
            if (!duplicateMine) {
                mines.push({ row, col });
            }
        }
    }

    function handleCellClick(event) {
        if (gameOver) return;

        const cell = event.target;
        const row = parseInt(cell.dataset.row);
        const col = parseInt(cell.dataset.col);

        if (cell.classList.contains('opened') || cell.classList.contains('flag')) return;

        if (mines.some(mine => mine.row === row && mine.col === col)) {
            revealMines();
            endGame(false);
        } else {
            const mineCount = countAdjacentMines(row, col);
            if (mineCount > 0) {
                cell.textContent = mineCount;
            } else {
                revealEmptyCells(row, col);
            }

            cell.classList.add('opened');
            openedCells++;

            if (openedCells === gridSize * gridSize - totalMines) {
                endGame(true);
            }
        }
    }

    function handleCellRightClick(event) {
        if (gameOver) return;

        event.preventDefault();

        const cell = event.target;

        if (!cell.classList.contains('opened')) {
            cell.classList.toggle('flag');
            flags += cell.classList.contains('flag') ? 1 : -1;
            updateMineCountDisplay();
        }
    }

    function countAdjacentMines(row, col) {
        const neighbors = [
            { row: row - 1, col: col - 1 },
            { row: row - 1, col: col },
            { row: row - 1, col: col + 1 },
            { row: row, col: col - 1 },
            { row: row, col: col + 1 },
            { row: row + 1, col: col - 1 },
            { row: row + 1, col: col },
            { row: row + 1, col: col + 1 }
        ];

        return neighbors.filter(neighbor => {
            return (
                neighbor.row >= 0 &&
                neighbor.row < gridSize &&
                neighbor.col >= 0 &&
                neighbor.col < gridSize &&
                mines.some(mine => mine.row === neighbor.row && mine.col === neighbor.col)
            );
        }).length;
    }

    function revealEmptyCells(row, col) {
        const queue = [{ row, col }];

        while (queue.length > 0) {
            const { row, col } = queue.shift();
            const cell = document.querySelector(`.cell[data-row="${row}"][data-col="${col}"]`);

            if (!cell || cell.classList.contains('opened') || cell.classList.contains('flag')) continue;

            const mineCount = countAdjacentMines(row, col);
            if (mineCount > 0) {
                cell.textContent = mineCount;
            } else {
                const neighbors = [
                    { row: row - 1, col: col - 1 },
                    { row: row - 1, col: col },
                    { row: row - 1, col: col + 1 },
                    { row: row, col: col - 1 },
                    { row: row, col: col + 1 },
                    { row: row + 1, col: col - 1 },
                    { row: row + 1, col: col },
                    { row: row + 1, col: col + 1 }
                ];

                queue.push(...neighbors.filter(neighbor => {
                    return (
                        neighbor.row >= 0 &&
                        neighbor.row < gridSize &&
                        neighbor.col >= 0 &&
                        neighbor.col < gridSize
                    );
                }));
            }

            cell.classList.add('opened');
            openedCells++;
        }
    }

    function revealMines() {
        mines.forEach(mine => {
            const cell = document.querySelector(`.cell[data-row="${mine.row}"][data-col="${mine.col}"]`);
            cell.classList.add('mine');
        });
    }

    function endGame(isWinner) {
        gameOver = true;
        restartButton.style.display = 'block';

        if (isWinner) {
            resultDisplay.textContent = 'Ви виграли!';
        } else {
            resultDisplay.textContent = 'Гра завершена. Ви програли.';
        }
    }

    function updateMineCountDisplay() {
        resultDisplay.textContent = `Прапорці: ${flags} / Міни: ${totalMines}`;
    }

    initializeGame();

    restartButton.addEventListener('click', function() {
        minesweeper.innerHTML = '';
        initializeGame();
    });
});