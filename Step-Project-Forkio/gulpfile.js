//Основный модуль
import gulp from "gulp";// импортируем в проект
//Импорт путей 
import { path } from "./gulp/config/path.js"; // и пути из папки gulp

// Импорт общих плагинов
import { plugins } from "./gulp/config/plugins.js";
//Передаем значения в глобальную переменую
 global.app = {
     path: path,
     gulp: gulp,
     plugins: plugins
 }



 //Импорт задачи
 import { reset } from "./gulp/tasks/reset.js";
 import { html } from "./gulp/tasks/html.js";
 import { server } from "./gulp/tasks/server.js";
 import { scss } from "./gulp/tasks/scss.js";
 import { js } from "./gulp/tasks/js.js";
 import { images } from "./gulp/tasks/images.js";
 import { otfToTtf, ttfToWoff, fontsStyle } from "./gulp/tasks/fonts.js";


//Функция wacher где будем собирать различные наблюдатели
function watcher () {
    gulp.watch(path.watch.html, html);
    gulp.watch(path.watch.scss, scss);
    gulp.watch(path.watch.js, js);
    gulp.watch(path.watch.images, images);
}

const fonts = gulp.series(otfToTtf, ttfToWoff, fontsStyle);
const mainTasks = gulp.parallel(html, scss, js, images) // перечисляем задачи которые нам одновременно нужно выполнить

//Выполняем сценарий задачи по функции watcher ,
// gulp настраиваем в режыме розработчика и продакшына
// series() - метод виполняет задачи последовательнно( copy, watcher)
const dev = gulp.series(reset, mainTasks, gulp.parallel(watcher, server))

 //Выполнение  сценария по умолчанию
 gulp.task('default', dev);
