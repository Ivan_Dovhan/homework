
import fileinclude from "gulp-file-include";
import webpHtmlNosvg from "gulp-webp-html-nosvg"
import versionNumber from "gulp-version-number"
import pug from "gulp-pug"
export const html = () => {
    return app.gulp.src(app.path.src.html)// доступ к нужным файлам
    .pipe(fileinclude()) //визаваем файл инклюд
    // .pipe(pug({
    //     // Сжатие html файлов
    //     pretty: true,
    //     // Показывать в терминале какой файл обработан
    //     verbose: true,
    // }))
    .pipe(app.plugins.replace(/@img\//g, 'img/')) // меняем путь картинки на нужный
    .pipe(webpHtmlNosvg())
    .pipe(
        versionNumber({
            'value': '%DT%',
            'append': {
                'key': '_v',
                'cover': 0,
                'to': [
                    'css',
                    'js',
                ]
            },
            'output': {
                'file': 'gulp/version.json'
            }
        })
    )
    .pipe(app.gulp.dest(app.path.build.html)) // действие, куда мы переносим файлы
    .pipe(app.plugins.browsersync.stream()) // Обоновление браузера
}