// Получаем имя проукта
//import { src } from 'gulp';
import * as nodePath from 'path';
const rootFolder = nodePath.basename(nodePath.resolve()); // получаем имя проекта

const buildFolder = `./dist`;
const srcFolder = `./src`;
// (чтобы использовать эти пути в других папках , нам нужно export)
export const path = { // информацыя о пути к тому или иному файлу
    build: {
        images: `${buildFolder}/img/`,
        js: `${buildFolder}/js/`,
        css: `${buildFolder}/css/`,
        html: `${buildFolder}/`,
        fonts: `${buildFolder}/fonts/`
    },
    src: {
        images: `${srcFolder}/img/**/*.{jpg,jpen,png,gif,webp}`,
        svg: `${srcFolder}/img/**/*.svg`,
        js: `${srcFolder}/js/app.js`,
        scss: `${srcFolder}/scss/style.scss`,
        html: `${srcFolder}/*.html`, // .pug // копирует html файли которые в корне
    },
    watch: {
        images: `${srcFolder}/img/**/*.{jpg,jpen,png,gif,webp,svg,ico}`,
        js: `${srcFolder}/js/**?*.js`,
        scss: `${srcFolder}/scss/**/*.scss`,
        html: `${srcFolder}/**/*.html`, // .pug //(следим за всеми файлами html в папке src,  )
    },
    clean: buildFolder,
    buildFolder: buildFolder,
    buildFolder: buildFolder,
    srcFolder: srcFolder,
    rootFolder: rootFolder,
    ftp: ``
}