let displayValue = '0';
        let operator = null;
        let memoryValue = 0;

        function updateDisplay() {
            document.getElementById('display').textContent = displayValue;
        }

        function appendDigit(digit) {
            if (displayValue === '0') {
                displayValue = digit;
            } else {
                displayValue += digit;
            }
            updateDisplay();
        }

        function setOperator(newOperator) {
            if (operator !== null) {
                calculateResult();
            }
            operator = newOperator;
            displayValue = '0';
        }

        function clearDisplay() {
            displayValue = '0';
            updateDisplay();
        }

        function calculateResult() {
            const currentValue = parseFloat(displayValue);
            let result;

            switch (operator) {
                case '+':
                    result = memoryValue + currentValue;
                    break;
                case '-':
                    result = memoryValue - currentValue;
                    break;
                case '*':
                    result = memoryValue * currentValue;
                    break;
                case '/':
                    result = memoryValue / currentValue;
                    break;
                default:
                    result = currentValue;
                    break;
            }

            displayValue = result.toString();
            updateDisplay();

            operator = null;
        }

        function addToMemory() {
            const currentValue = parseFloat(displayValue);
            memoryValue += currentValue;
            updateDisplay();
        }

        function subtractFromMemory() {
            const currentValue = parseFloat(displayValue);
            memoryValue -= currentValue;
            updateDisplay();
        }

        function recallMemory() {
            displayValue = memoryValue.toString();
            updateDisplay();
        }