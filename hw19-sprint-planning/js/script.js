function calculateDeadlineCompletionTime(teamSpeed, backlog, deadline) {
    const workHoursPerDay = 8;
    const workingDays = 5;

    const pointsPerDay = teamSpeed.reduce((sum, speed) => sum + speed, 0);

    const totalBacklogPoints = backlog.reduce((sum, points) => sum + points, 0);

    const daysNeeded = Math.ceil(totalBacklogPoints / pointsPerDay);

    const extraHoursAfterDeadline = (daysNeeded - workingDays) * workHoursPerDay;

    if (daysNeeded <= workingDays) {
        console.log(`Усі завдання будуть успішно виконані за ${daysNeeded} днів до настання дедлайну!`);
    } else {
        console.log(`Команді розробників доведеться витратити додатково ${extraHoursAfterDeadline} годин після дедлайну, щоб виконати всі завдання в беклозі.`);
    }
}

const teamSpeed = [3, 2, 4]; 
const backlog = [10, 8, 12]; 
const deadline = new Date('2023-12-15'); 

calculateDeadlineCompletionTime(teamSpeed, backlog, deadline);