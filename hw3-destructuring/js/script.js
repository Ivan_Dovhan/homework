// Завдання 1
// Дві компанії вирішили обєднатись, і для цього їм потрібно обєднати базу даних своїх клієнтів.
// У вас є 2 масиви рядків, у кожному з них – прізвища клієнтів.
//  Створіть на їх основі один масив, який буде обєднання двох масивів без повторюваних прізвищ клієнтів.

const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

const uniqueClients = [...new Set(clients1.concat(clients2))];
console.log(uniqueClients);



// Завдання 2
// Перед вами массив characters, що складається з обєктів. Кожен обєкт описує одного персонажа.
// Створіть на його основі масив charactersShortInfo, що складається з обєктів, у яких є тільки 3 поля - імя, прізвище та вік


const characters = [
    {
      name: "Елена",
      lastName: "Гилберт",
      age: 17, 
      gender: "woman",
      status: "human"
    },
    {
      name: "Кэролайн",
      lastName: "Форбс",
      age: 17,
      gender: "woman",
      status: "human"
    },
    {
      name: "Аларик",
      lastName: "Зальцман",
      age: 31,
      gender: "man",
      status: "human"
    },
    {
      name: "Дэймон",
      lastName: "Сальваторе",
      age: 156,
      gender: "man",
      status: "vampire"
    },
    {
      name: "Ребекка",
      lastName: "Майклсон",
      age: 1089,
      gender: "woman",
      status: "vempire"
    },
    {
      name: "Клаус",
      lastName: "Майклсон",
      age: 1093,
      gender: "man",
      status: "vampire"
    }
  ];

const charactersShortInfo = characters.map(({ name, surname, age }) => ({ name, surname, age }));
console.log(charactersShortInfo);


// Напишіть деструктуруюче присвоєння, яке:

// властивість name присвоїть в змінну імя
// властивість years присвоїть в змінну вік
// властивість isAdmin присвоює в змінну isAdmin false, якщо такої властивості немає в обєкті
// Виведіть змінні на екран.

const user1 = {
  name: "John",
  years: 30
};

const { name: імя, years: вік, isAdmin = false } = user1;

console.log(імя); 
console.log(вік); 
console.log(isAdmin); 

// Детективне агентство кілька років збирає інформацію про можливу особистість Сатоши Накамото. Вся інформація, зібрана у конкретному році, зберігається в окремому обєкті. 
// Усього таких обєктів три - satoshi2018, satoshi2019, satoshi2020.
// Щоб скласти повну картину та профіль, вам необхідно обєднати дані з цих трьох обєктів в один обєкт - fullProfile.
// Зверніть увагу, що деякі поля в обєктах можуть повторюватися.
// У такому випадку в результуючому обєкті має зберегтися значення, яке було отримано пізніше (наприклад, значення з 2020 пріоритетніше порівняно з 2019).
// Напишіть код, який складе повне досьє про можливу особу Сатоші Накамото. Змінювати обєкти satoshi2018, satoshi2019, satoshi2020 не можна.

// const fullProfile = {
//   ...satoshi2018,
//   ...satoshi2019,
//   ...satoshi2020
// };

// console.log(fullProfile);


// Дано масив книг. Вам потрібно додати до нього ще одну книгу, не змінюючи існуючий масив (в результаті операції має бути створено новий масив).

const books = [{
  name: 'Harry Potter',
  author: 'J.K. Rowling'
}, {
  name: 'Lord of the rings',
  author: 'J.R.R. Tolkien'
}, {
  name: 'The witcher',
  author: 'Andrzej Sapkowski'
}];

const bookToAdd = {
  name: 'Game of thrones',
  author: 'George R. R. Martin'
}
const booksWithNewBook = [...books, bookToAdd];

// Даний обєкт employee. Додайте до нього властивості age і salary,
// не змінюючи початковий обєкт (має бути створено новий обєкт, який включатиме всі необхідні властивості).
// Виведіть новий обєкт у консоль.

const employeeWithAgeAndSalary = {
  name: 'Vitalii',
  surname: 'Klichko',
  age: 45,
  salary: 1000000
  }
  
  console.log(employeeWithAgeAndSalary);


  // Доповніть код так, щоб він коректно працював

  const array = ['value', () => 'showValue'];
  alert(array[0]); 
  alert(array[1]()); 