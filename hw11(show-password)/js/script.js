let eyes = document.querySelectorAll(".fas");
eyes.forEach((eye) => {
  eye.addEventListener("click", () => {
    eye.classList.toggle("fa-eye-slash");
    let input = eye.previousElementSibling;
    if (input.type === "password") {
      input.type = "text";
    } else {
      input.type = "password";
    }
  });
});

let p = document.createElement("p");
p.style.color = "red";
let btn = document.querySelector(".btn");
btn.before(p);

let form = document.querySelector(".password-form");
form.addEventListener("submit", (event) => {
  event.preventDefault();
  let password = form.password;
  let checkPassword = form.check_password;
  if (password.value === checkPassword.value) {
    alert("You are welcome");
    p.innerHTML = "";
  } else {
    p.innerHTML = "Потрібно ввести однакові значення";
  }
});

