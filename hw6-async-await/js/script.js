async function getIPInfo() {
    try {
      // виконуємо запит за IP адресою клієнта
      const response1 = await fetch('https://api.ipify.org/?format=json');
      const { ip } = await response1.json();
      
      // виконуємо запит за інформацією про фізичну адресу
      const response2 = await fetch(`https://ip-api.com/json/${ip}`);
      const { continent, country, regionName, city, district } = await response2.json();
      
      // виводимо інформацію на сторінку
      const infoDiv = document.getElementById('info');
      infoDiv.innerHTML = `
        <p>Континент: ${continent}</p>
        <p>Країна: ${country}</p>
        <p>Регіон: ${regionName}</p>
        <p>Місто: ${city}</p>
        <p>Район: ${district}</p>
      `;
    } catch (error) {
      console.error(error);
    }
  }
  const findIpButton = document.getElementById('find-ip-button');
  findIpButton.addEventListener('click', getIPInfo);